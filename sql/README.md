

sn - VARCHAR(255) primary key   
  Serial Number is the primary key   

name - VARCHAR(255)   
  store variable-length digits limited to 255

descrition - TEXT    
  store long-form text strings that can take from 1 byte to 4 GB    

value - DECIMAL(15,2)   
  15 is the precision (total length of value including decimal places)   
  2 is the number of digits after decimal point   

  Since money needs an exact representation, don't use data types that are only approximate like float   
  You can use a fixed-point numeric data type decimal(15,2)   

picture - LONGBLOG   
  storing images in the database is not recommended.  it is better to reference a path name to the image instead.  

json - JSON  
  store JavaScript Object Notation  {"image_path": "/tmp/image.png", "b2sum": "18ddbf2eda4b3a976ce5346dac6320e68a70bd79"}    

note - TEXT
  store long-form text strings that can take from 1 byte to 4 GB    

created - DATETIME   

modified - DATETIME ON UPDATE CURRENT_TIMESTAM  
  Timestamps in MySQL are generally used to track changes to records,  
  and are often updated every time the record is changed.  

FULLTEXT (name, description, note)   
  A FULLTEXT index is a special type of index that finds keywords in the text instead of comparing the values to the values in the index.  
  
---   

Full-text search is a technique to search for documents that don’t perfectly match the search criteria.   
Full-text searches have three modes: the natural language mode, the boolean mode, and the query expansion mode.   
https://dev.mysql.com/doc/refman/8.0/en/fulltext-search.html
https://mariadb.com/kb/en/full-text-index-overview/

TIMESTAMP date type changes the values after changing the time-zone to america/new_work,  
where DATETIME is unchanged.   
https://dev.mysql.com/doc/refman/8.0/en/timestamp-initialization.html    


